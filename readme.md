# Graphical Set

An iOS game written in Swift and UIKit during my [Stanford's CS193p online course](https://cs193p.sites.stanford.edu/) self-study for "[Mazowsze - stypendia dla uczniów szkół zawodowych](https://stypendiazawodowe.oeiizk.waw.pl/)" project.

There are 3 game modes implemented, you can either:

-   Play alone
-   Play against a second player (on the same device)
-   Play against a computer (it uses a very simple "hint a set" algorithm to select the cards)

![game preview](preview.png)

## Mechanics

The deck consists of 81 unique cards that vary in four features across three possibilities for each kind of feature: number of shapes (one, two or three), shape (diamond, squiggle, oval), shading (solid, striped or open), and color (red, green or purple).

Each possible combination of features (e.g. a card with three striped green diamonds) appears as a card precisely once in the deck.

In the game, certain combinations of three cards are said to make up a set. For each one of the four categories of features - color, number, shape and shading - the three cards must display that feature as a) either all the same or b) all different.

Player needs to tap the "Take turn" button to signal that they're ready to pick cards. If a) the three selected cards aren't a set or b) the player doesn't pick any card within the turn time, the other player automatically gets a turn with doubled time to choose cards.

There's also a score penalty system implemented:

-   if the player tries to guess (takes turns too fast and chooses wrong cards every time), 2, 3 or 9 points are subtracted from his score
-   if the player taps "Deal 3 More Cards" while there is a set on the table, 4 points are subtracted from his score
