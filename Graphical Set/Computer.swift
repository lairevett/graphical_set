//
//  Computer.swift
//  Set
//
//  Created by Marcin Sadowski on 10/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import Foundation

class Computer: Player {
    enum State {
        case none, thinking, seesMatch, lost, won
    }
    
    private weak var game: Game!
    private var baseThinkingTime: TimeInterval = 50.0
    private var findingMatchTimer: Timer?
    private var selectingTimer: Timer?
    private var waitingForTurnTimer: Timer?
    
    var state = State.none
    var isAttached: Bool { game != nil }
    
    deinit {
        invalidateTimers()
    }
    
    private func startSelectingCards(at indices: [Int]) {
        game.currentPlayer = self
        
        // onSelectCard() needs to be called four times with card index to match.
        let iterator = indices.makeInfiniteIterator()
        
        self.selectingTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            if self._selectedCards.count == 3 {
                timer.invalidate()
            }
            
            self.game.onSelectCard(at: iterator.next()!)
            self.game.forceUpdate()
        }
        
        selectingTimer!.tolerance = 0.5
    }
    
    private func scheduleFindingMatchTimer(minusTimeInterval: TimeInterval = 0.0, rememberedCardsIndices: [Int] = []) {
        assert(game != nil, "Computer wasn't attached to game.")
        if rememberedCardsIndices.isEmpty {
            state = .thinking
            
            findingMatchTimer = Timer.scheduledTimer(withTimeInterval: baseThinkingTime - minusTimeInterval, repeats: false) { _ in
                if let firstMatch = self.grabFirstMatch(from: self.game.cardsInGame) {
                    self.state = .seesMatch
                    if self.game.currentPlayer == nil {
                        self.startSelectingCards(at: firstMatch)
                    } else {
                        self.invalidateTimers()
                        self.scheduleFindingMatchTimer(rememberedCardsIndices: firstMatch)
                    }
                } else {
                    self.invalidateTimers()
                    self.scheduleFindingMatchTimer(minusTimeInterval: 30)
                }
            }
            
            findingMatchTimer!.tolerance = 20.0
        } else {
            let rememberedCards = rememberedCardsIndices.map { game.cardsInGame[$0] }
            
            if Game.checkIfAllPropertiesMatch(of: rememberedCards) {
                if self.game.currentPlayer == nil {
                    self.invalidateTimers()
                    self.startSelectingCards(at: rememberedCardsIndices)
                } else {
                    // Wait two seconds to check again if player didn't take turn.
                    waitingForTurnTimer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { _ in
                        self.scheduleFindingMatchTimer(rememberedCardsIndices: rememberedCardsIndices)
                    }
                }
            } else {
                // If player already picked one of the remembered
                // cards / all of them, start searching for a new match.
                self.invalidateTimers()
                self.scheduleFindingMatchTimer()
            }
        }
    }
    
    private func scheduleFindingMatchTimer(withTemporaryState temporaryState: State) {
        state = temporaryState
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { _ in
            self.scheduleFindingMatchTimer()
        }
    }
    
    func grabFirstMatch(from cards: [Card]) -> [Int]? {
        func search(_ firstPivotPosition: Int, _ secondPivotPosition: Int, _ thirdPivotPosition: Int) -> [Int]? {
            if thirdPivotPosition == cards.count {
                if secondPivotPosition == cards.count - 1 {
                    if firstPivotPosition == cards.count - 2 {
                        // Pivots are next to each other at the end, there weren't any matches.
                        return nil
                    } else {
                        // Second pivot is at cardsOnTable count - 1.
                        // Move first pivot one card to right, and put second and third pivots next to it.
                        return search(firstPivotPosition + 1, firstPivotPosition + 2, firstPivotPosition + 3)
                    }
                } else {
                    // Third pivot is outside of cardsOnTable array.
                    // Move second pivot one card to right, and put third pivot next to it.
                    return search(firstPivotPosition, secondPivotPosition + 1, secondPivotPosition + 2)
                }
            } else {
                // Check if match for every pivot move.
                let pivots = [firstPivotPosition, secondPivotPosition, thirdPivotPosition]
                let cardsAtPivots = pivots.map { cards[$0] }
                
                if Game.checkIfAllPropertiesMatch(of: cardsAtPivots) {
                    return pivots
                }
                
                return search(firstPivotPosition, secondPivotPosition, thirdPivotPosition + 1)
            }
        }

        return search(0, 1, 2)
    }
    
    override func onMatch() {
        super.onMatch()
        scheduleFindingMatchTimer(withTemporaryState: .won)
    }
    
    override func onOpponentWin() {
        super.onOpponentWin()
        invalidateTimers()
        scheduleFindingMatchTimer(withTemporaryState: .lost)
    }
    
    func attach(to game: Game) {
        self.game = game
        scheduleFindingMatchTimer()
    }
    
    func detach() {
        invalidateTimers()
        self.game = nil
    }
    
    func invalidateTimers() {
        if let timer = findingMatchTimer {
            timer.invalidate()
        }
        
        if let timer = selectingTimer {
            timer.invalidate()
        }
        
        if let timer = waitingForTurnTimer {
            timer.invalidate()
        }
    }
}
