//
//  Deck.swift
//  Set
//
//  Created by Marcin Sadowski on 09/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import Foundation

struct Deck {
    private(set) var cards = [Card]()
    
    init() {
        for number in 1...3 {
            for shape in Card.Shape.allCases {
                for color in Card.Color.allCases {
                    for shading in Card.Shading.allCases {
                        cards.append(Card(number: number, shape: shape, color: color, shading: shading))
                    }
                }
            }
        }
        
        assert(cards.count == 81, "There are \(cards.count) cards in deck, required: 81.")
    }
    
    mutating func shuffle() {
        cards.shuffle()
    }
    
    mutating func draw(amount: Int) -> [Card] {
        if cards.count >= amount {
            let drawnCards = cards[0..<amount]
            cards.removeFirst(amount)
            
            return Array(drawnCards)
        }

        return [] // Nothing to draw.
    }
    
    mutating func putBack(_ cards: inout [Card]) {
        self.cards += cards
        cards.removeAll()
    }
}
