//
//  ViewController.swift
//  Graphical Set
//
//  Created by Marcin Sadowski on 11/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet private weak var menuStackView: UIStackView!
    @IBOutlet private weak var gameSessionBoardView: GameBoardView! {
        didSet {
            gameSessionBoardView.makeTapCardGestureRecognizer = makeTapCardGestureRecognizer

            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(onBoardSwipeDown(_:)))
            swipeDown.direction = .down
            gameSessionBoardView.addGestureRecognizer(swipeDown)
            
            let rotation = UIRotationGestureRecognizer(target: self, action: #selector(onBoardRotation(_: )))
            gameSessionBoardView.addGestureRecognizer(rotation)
        }
    }
    
    @IBOutlet private weak var gameSessionScoresStackView: UIStackView!
    private var gameSessionScoreLbls: (left: UILabel, right: UILabel) {
        get {
            return (
                left: gameSessionScoresStackView.subviews[0] as! UILabel,
                right: gameSessionScoresStackView.subviews[1] as! UILabel
            )
        }
    }
    
    @IBOutlet private weak var gameSessionTurnStackView: UIStackView!
    private var areGameSessionTurnBtnsEnabled: Bool {
        get { gameSessionTurnStackView.subviews.allSatisfy { ($0 as! UIButton).isEnabled } }
        set {
            for case let btn as UIButton in gameSessionTurnStackView.subviews {
                btn.isEnabled = newValue
            }
        }
    }
    
    @IBOutlet private weak var gameSessionMenuStackView: UIStackView!
    private var gameSessionMenuBtns: (newGame: UIButton, hint: UIButton, deal3Cards: UIButton) {
        get {
            return (
                newGame: gameSessionMenuStackView.subviews[0] as! UIButton,
                hint: gameSessionMenuStackView.subviews[1] as! UIButton,
                deal3Cards: gameSessionMenuStackView.subviews[2] as! UIButton
            )
        }
    }
    
    private lazy var game = Game(onCardsInGameChange: [gameSessionBoardView.update, update])
    
    private var isGameVisible: Bool {
        get { menuStackView.isHidden }
        set {
            menuStackView.isHidden = newValue
            gameSessionBoardView.isHidden = !newValue
            gameSessionScoresStackView.isHidden = !newValue
            if game.sessionMode == .alone {
                gameSessionScoreLbls.right.isHidden = newValue
            }
            
            if game.sessionMode == .withSecondPlayer {
                gameSessionTurnStackView.isHidden = !newValue
            } else if game.sessionMode == .withComputer {
                gameSessionTurnStackView.isHidden = !newValue
                (gameSessionTurnStackView.subviews[1] as! UIButton).isHidden = newValue
            }
            
            gameSessionMenuStackView.isHidden = !newValue
            
            update()
        }
    }
    
    @IBAction private func touchPlayAloneBtn() {
        game.startSession(.alone)
        isGameVisible = true
    }
    
    @IBAction private func touchPlayWithSecondPlayerBtn() {
        game.startSession(.withSecondPlayer)
        isGameVisible = true
    }
    
    @IBAction private func touchPlayWithComputerBtn() {
        game.startSession(.withComputer)
        isGameVisible = true
    }
    
    @IBAction private func touchLeftTakeTurnBtn() {
        game.currentPlayer = game.players[0]
        update()
    }
    
    @IBAction private func touchRightTakeTurnBtn() {
        game.currentPlayer = game.players[1]
        update()
    }
    
    @IBAction private func touchNewGameBtn() {
        isGameVisible = false
        game.endSession()
    }
    
    @IBAction private func touchHintBtn() {
        if !gameSessionBoardView.isAnyHintEnabled, let match = game.computer.grabFirstMatch(from: game.cardsInGame) {
            for index in match {
                (gameSessionBoardView.subviews[index] as! GameCardView).highlightHint()
            }
        }
    }
    
    @IBAction private func touchDeal3CardsBtn() {
        if game.isPlayerTurn {
            game.putCardsInGame(amount: 3)
            
            if game.deck.cards.count - 3 < 0 {
                gameSessionMenuBtns.deal3Cards.isEnabled = false
            }
        }
    }
    
    private func update(cards: [Card]? = nil) {
        if game.cardsInGame.count > 0 {
            for index in game.cardsInGame.indices {
                (gameSessionBoardView.subviews[index] as! GameCardView).clearHighlights()
            }
            
            if let player = game.currentPlayer {
                for index in player.getSelectedCardsIndices(from: game.cardsInGame) {
                    let cardView = gameSessionBoardView.subviews[index] as! GameCardView
                    
                    cardView.highlightSelect()
                    if player.isMatchSelected {
                        cardView.highlightMatch()
                    }
                }
                
                areGameSessionTurnBtnsEnabled = false
            } else {
                areGameSessionTurnBtnsEnabled = true
            }
            
            if game.players.count == 1 {
                gameSessionScoreLbls.left.text = "Score: \(game.players[0].score)"
            } else if game.players.count == 2 {
                let scorePrefixes: (left: String, right: String)
                
                var computerStatusPrefix = ""
                if game.computer.isAttached {
                    switch game.computer.state {
                    case .none: computerStatusPrefix = ""
                    case .thinking: computerStatusPrefix = "🤔 "
                    case .seesMatch: computerStatusPrefix = "😁 "
                    case .won: computerStatusPrefix = "😂 "
                    case .lost: computerStatusPrefix = "😢 "
                    }
                }

                if let player = game.currentPlayer {
                    if player == game.players[0] {
                        scorePrefixes = ("P1*:", "P2:")
                    } else {
                        scorePrefixes = ("P1:", "P2*:")
                    }
                } else {
                    scorePrefixes = ("P1:", "P2:")
                }
                
                gameSessionScoreLbls.left.text = "\(scorePrefixes.left) \(game.players[0].score)"
                gameSessionScoreLbls.right.text = "\(computerStatusPrefix)\(scorePrefixes.right) \(game.players[1].score)"
            }
                
            let match = game.computer.grabFirstMatch(from: game.cardsInGame)
            gameSessionMenuBtns.hint.isEnabled = match != nil
        }
    }
    
    @objc private func onBoardSwipeDown(_ sender: UISwipeGestureRecognizer) {
        switch sender.state {
        case .ended: touchDeal3CardsBtn()
        default: break
        }
    }
    
    @objc private func onBoardRotation(_ sender: UIRotationGestureRecognizer) {
        switch sender.state {
        case .ended:
            if game.isPlayerTurn {
                game.cardsInGame.shuffle()
            }
        default: break
        }
    }
    
    @objc private func onTapCardFromBoard(_ sender: UITapGestureRecognizer) {
        switch sender.state {
        case .ended:
            if game.isPlayerTurn {
                let cardView = sender.view as! GameCardView
                let index = game.cardsInGame.firstIndex(of: cardView.representation)!
                game.onSelectCard(at: index)
                update()
            }
        default: break
        }
    }
    
    private func makeTapCardGestureRecognizer() -> UITapGestureRecognizer {
        UITapGestureRecognizer(target: self, action: #selector(onTapCardFromBoard(_:)))
    }
}

extension Array {
    func makeInfiniteIterator() -> AnyIterator<Element> {
        var index = self.startIndex
        
        return AnyIterator({
            if self.isEmpty {
                return nil
            }
            
            let element = self[index]
            
            self.formIndex(after: &index)
            if index == self.endIndex {
                index = self.startIndex
            }
            
            return element
        })
    }
}
