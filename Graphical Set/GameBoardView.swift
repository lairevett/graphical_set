//
//  GameBoardView.swift
//  Graphical Set
//
//  Created by Marcin Sadowski on 11/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import UIKit

class GameBoardView: UIView {
    var makeTapCardGestureRecognizer: (() -> UITapGestureRecognizer)?
    
    private var grid: Grid!
        
    private var gridElementAspectRatio: CGSize {
        get {
            let ratio = CGSize(width: 2, height: 3.5)
            
            if bounds.width > bounds.height {
                return ratio.inverted()
            }
            
            return ratio
        }
    }
    
    var isAnyHintEnabled: Bool {
        for subview in subviews {
            if subview.backgroundColor == GameCardView.hintBackgroundColor {
                return true
            }
        }
        
        return false
    }
    
    private func setupGrid(withCellCount cellCount: Int) {
        grid = Grid(layout: .aspectRatio(gridElementAspectRatio.width / gridElementAspectRatio.height), frame: bounds)
        grid.cellCount = cellCount
    }
    
    private func makeCardView(withRepresentation representation: Card) -> GameCardView {
        let cardView = GameCardView()
        cardView.contentMode = .redraw
        cardView.backgroundColor = UIColor.white
        cardView.layer.borderColor = UIColor.systemGray.cgColor
        cardView.representation = representation
        if let makeGestureRecognizer = makeTapCardGestureRecognizer {
            cardView.addGestureRecognizer(makeGestureRecognizer())
        }
        return cardView
    }
    
    private func refreshCardsRepresentations(newRepresentations cards: [Card]) {
        for index in cards.indices {
            (subviews[index] as! GameCardView).representation = cards[index]
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupGrid(withCellCount: subviews.count)
        
        for index in subviews.indices {
            subviews[index].frame = grid[index]!
        }
    }
        
    func update(cards: [Card]) {
        let difference = cards.count - grid.cellCount

        if difference < 0, cards.count > 0 {
            setupGrid(withCellCount: cards.count)
            for _ in 0..<abs(difference) {
                subviews[0].removeFromSuperview()
            }
            
            refreshCardsRepresentations(newRepresentations: cards)
        } else if difference > 0 {
            let endOfGrid = grid.cellCount
            
            setupGrid(withCellCount: cards.count)
            for index in 0..<difference {
                let cardView = makeCardView(withRepresentation: cards[endOfGrid + index])
                cardView.frame = grid[endOfGrid + index]!
                addSubview(cardView)
            }
        } else {
            refreshCardsRepresentations(newRepresentations: cards)
        }
    }
}
