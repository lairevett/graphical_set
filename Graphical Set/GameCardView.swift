//
//  GameCardView.swift
//  Graphical Set
//
//  Created by Marcin Sadowski on 11/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import UIKit

class GameCardView: UIView {
    private var shapeToCardRatio: CGSize {
        get {
            let ratio = CGSize(width: 0.75, height: 0.25)
            
            if bounds.width > bounds.height {
                return ratio.inverted()
            }
            
            return ratio
        }
    }
    
    private let frameSizeToGapBetweenShapesRatio: CGFloat = 12.5
    private let stripeWidthToShapeWidthRatio: CGFloat = 0.05
    private let strideBetweenStripesToShapeWidthRatio: CGFloat = 0.1
    private var gapBetweenShapes: CGSize { frame.size / frameSizeToGapBetweenShapesRatio }
    
    static let backgroundColor = UIColor.white
    static let hintBackgroundColor = UIColor.systemYellow
    static let matchBackgroundColor = UIColor.systemGreen
    static let borderColor = UIColor.systemGray
    static let borderLineWidth: CGFloat = 1.0
    
    var representation = Card(number: 3, shape: .squiggle, color: .purple, shading: .striped) {
        didSet { setNeedsDisplay() }
    }
    
    private func getColor() -> UIColor {
        switch representation.color {
        case .red: return UIColor.red
        case .green: return UIColor.green
        case .purple: return UIColor.purple
        }
    }
    
    private func makeDiamondShapePath() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: bounds.midX, y: bounds.minY))
        
        path.addLine(to: CGPoint(x: bounds.width, y: bounds.midY))
        path.addLine(to: CGPoint(x: bounds.width / 2.0, y: bounds.maxY))
        path.addLine(to: CGPoint(x: bounds.minX, y: bounds.height / 2.0))
        path.close()
        
        path.apply(CGAffineTransform(scaleX: shapeToCardRatio.width, y: shapeToCardRatio.height))
        return path
    }
    
    private func makeOvalShapePath() -> UIBezierPath {
        let rect = CGRect(origin: CGPoint(x: bounds.midX, y: bounds.midY), size: bounds.size)
            .applying(CGAffineTransform(scaleX: shapeToCardRatio.width, y: shapeToCardRatio.height))
        
        return UIBezierPath(roundedRect: rect, cornerRadius: 32.0)
    }
    
    private func makeSquigglePath() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: bounds.width * 0.05, y: bounds.height * 0.35))

        path.addCurve(
            to: CGPoint(x: bounds.width * 0.55, y: bounds.height * 0.25),
            controlPoint1: CGPoint(x: bounds.width * 0.15, y: 0.0),
            controlPoint2: CGPoint(x: bounds.width * 0.3, y: 0.0)
        )
        
        path.addCurve(
            to: CGPoint(x: bounds.width * 0.9, y: bounds.height * 0.25),
            controlPoint1: CGPoint(x: bounds.width * 0.7, y: bounds.height * 0.35),
            controlPoint2: CGPoint(x: bounds.width * 0.8, y: bounds.height * 0.45)
        )
        
        path.addCurve(
            to: CGPoint(x: bounds.width * 0.45, y: bounds.height * 0.75),
            controlPoint1: CGPoint(x: bounds.width * 0.85, y: bounds.height),
            controlPoint2: CGPoint(x: bounds.width * 0.7, y: bounds.height)
        )
        
        path.addCurve(
            to: CGPoint(x: bounds.width * 0.1, y: bounds.height * 0.75),
            controlPoint1: CGPoint(x: bounds.width * 0.3, y: bounds.height * 0.65),
            controlPoint2: CGPoint(x: bounds.width * 0.2, y: bounds.height * 0.55)
        )
        
        path.addCurve(
            to: CGPoint(x: bounds.width * 0.05, y: bounds.height * 0.35),
            controlPoint1: CGPoint(x: 0.0, y: bounds.height * 0.65),
            controlPoint2: CGPoint(x: bounds.width * 0.04, y: bounds.height * 0.35)
        )
        
        if bounds.width > bounds.height {
            path.apply(
                CGAffineTransform(rotationAngle: -.pi / 2)
                    .scaledBy(x: bounds.height / bounds.width, y: bounds.width / bounds.height)
            )
        }
        
        path.apply(CGAffineTransform(scaleX: shapeToCardRatio.width, y: shapeToCardRatio.height))
        return path
    }
    
    private func getShapePath() -> UIBezierPath {
        let path: UIBezierPath
                
        switch representation.shape {
        case .diamond: path = makeDiamondShapePath()
        case .oval: path = makeOvalShapePath()
        case .squiggle: path = makeSquigglePath()
        }
        
        return path
    }
    
    private func getCenteredShapePath() -> UIBezierPath {
        let path = getShapePath()
        path.apply(CGAffineTransform(translationX: bounds.midX - path.bounds.midX, y: bounds.midY - path.bounds.midY))
        return path
    }
    
    private func drawStripes(for shapePath: UIBezierPath) {
        backgroundColor?.setFill() ?? UIColor.magenta.setFill()
        
        let stripe = CGRect(
            origin: shapePath.bounds.origin,
            size: CGSize(
                width: shapePath.bounds.width * stripeWidthToShapeWidthRatio,
                height: shapePath.bounds.height
            )
        )
        
        let stripePath = UIBezierPath(rect: stripe)
        
        let strideX = shapePath.bounds.width * strideBetweenStripesToShapeWidthRatio
        while (stripePath.currentPoint.x + stripePath.bounds.width) < shapePath.bounds.maxX {
            stripePath.fill()
            stripePath.apply(CGAffineTransform(translationX: strideX, y: 0.0))
        }
        
        getColor().setStroke()
        shapePath.stroke()
    }
    
    private func drawShape(fromPath path: UIBezierPath) {
        switch representation.shading {
        case .solid:
            getColor().setFill()
            path.fill()
        case .striped:
            getColor().setFill()
            path.fill()
            drawStripes(for: path)
        case .open:
            getColor().setStroke()
            path.stroke()
        }
    }
    
    private func drawOneShape() {
        drawShape(fromPath: getCenteredShapePath())
    }
    
    private func drawTwoShapes() {
        func getTranslations() -> (CGPoint, CGPoint) {
            if bounds.width > bounds.height {
                return (
                    CGPoint(x: -(path.bounds.width + gapBetweenShapes.width) / 2, y: 0.0),
                    CGPoint(x: path.bounds.width + gapBetweenShapes.width, y: 0.0)
                )
            } else {
                return (
                    CGPoint(x: 0.0, y: -(path.bounds.height + gapBetweenShapes.height) / 2),
                    CGPoint(x: 0.0, y: path.bounds.height + gapBetweenShapes.height)
                )
            }
        }
        
        let path = getCenteredShapePath()
        let translations = getTranslations()

        path.apply(CGAffineTransform(translationX: translations.0.x, y: translations.0.y))
        drawShape(fromPath: path)
        
        path.apply(CGAffineTransform(translationX: translations.1.x, y: translations.1.y))
        drawShape(fromPath: path)
    }
    
    private func drawThreeShapes() {
        func getTranslations() -> (CGPoint, CGPoint) {
            if bounds.width > bounds.height {
                return (
                    CGPoint(x: -path.bounds.width - gapBetweenShapes.width, y: 0.0),
                    CGPoint(x: (path.bounds.width + gapBetweenShapes.width) * 2, y: 0.0)
                )
            } else {
                return (
                    CGPoint(x: 0.0, y: -path.bounds.height - gapBetweenShapes.height),
                    CGPoint(x: 0.0, y: (path.bounds.height + gapBetweenShapes.height) * 2)
                )
            }
        }
        
        let path = getCenteredShapePath()
        drawShape(fromPath: path)
        
        let translations = getTranslations()
        path.apply(CGAffineTransform(translationX: translations.0.x, y: translations.0.y))
        drawShape(fromPath: path)
        
        path.apply(CGAffineTransform(translationX: translations.1.x, y: translations.1.y))
        drawShape(fromPath: path)
    }
    
    override func draw(_ rect: CGRect) {
        switch representation.number {
        case 1: drawOneShape()
        case 2: drawTwoShapes()
        case 3: drawThreeShapes()
        default: break
        }
    }
    
    func highlightHint() {
        backgroundColor = GameCardView.hintBackgroundColor
    }
    
    func highlightMatch() {
        backgroundColor = GameCardView.matchBackgroundColor
    }
    
    func highlightSelect() {
        layer.borderWidth = GameCardView.borderLineWidth
    }
    
    func clearHighlights() {
        backgroundColor = GameCardView.backgroundColor
        layer.borderWidth = 0
    }
}

extension CGSize {
    func inverted() -> CGSize { CGSize(width: height, height: width) }
    
    static func / (rhs: CGSize, lhs: CGFloat) -> CGSize {
        CGSize(width: rhs.width / lhs, height: rhs.height / lhs)
    }
}
