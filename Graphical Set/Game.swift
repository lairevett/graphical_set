//
//  Game.swift
//  Game
//
//  Created by Marcin Sadowski on 07/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import Foundation

class Game {
    enum SessionMode {
        case none, alone, withSecondPlayer, withComputer
    }
    
    private(set) var sessionMode = SessionMode.none
    
    private(set) var players = [Player]()
    private(set) var computer = Computer()
    
    private var turnTimer: Timer?
    private var nextTurnTime: TimeInterval = 8
    var isPlayerTurn: Bool { !(currentPlayer == nil || currentPlayer is Computer)  }

    var currentPlayer: Player! {
        didSet {
            // Schedule turn times when playing with computer or second player.
            if currentPlayer != nil, sessionMode != .alone {
                let lastMatchedCardsCount = matchedCards.count
                turnTimer = Timer.scheduledTimer(withTimeInterval: nextTurnTime, repeats: false) { _ in
                    if self.nextTurnTime > 8 {
                        self.nextTurnTime = 8
                    }
                    
                    // Don't change next turn time if playing with computer.
                    // Just cancel turn after nextTurnTime seconds.
                    if self.sessionMode == .withSecondPlayer, self.matchedCards.count == lastMatchedCardsCount {
                        self.nextTurnTime *= 2
                        self.currentPlayer._selectedCards.removeAll()
                        
                        // Change turns with added time.
                        let otherPlayer = self.players.first(where: { $0 != self.currentPlayer })
                        self.currentPlayer = otherPlayer
                    } else {
                        self.currentPlayer = nil
                    }
                    
                    self.forceUpdate()
                }
            }
        }
    }
    
    private(set) var deck = Deck()
    private(set) var matchedCards = [Card]()
    var cardsInGame = [Card]() { didSet { forceUpdate() } }
    
    private var onCardsInGameChange: [(_ cards: [Card]) -> Void]?
    
    init(onCardsInGameChange: [(_ cards: [Card]) -> Void]) {
        self.onCardsInGameChange = onCardsInGameChange
    }
    
    func startSession(_ sessionMode: SessionMode) {
        self.sessionMode = sessionMode
        deck.shuffle()
        putCardsInGame(amount: 12)
        
        switch sessionMode {
        case .alone:
            players.append(Player())
            currentPlayer = players[0]
        case .withSecondPlayer:
            players.append(contentsOf: [Player(), Player()])
        case .withComputer:
            computer.attach(to: self)
            players.append(contentsOf: [Player(), computer])
        default: break
        }
    }
    
    func endSession() {
        if let timer = turnTimer {
            timer.invalidate()
        }
        
        currentPlayer = nil
        players.removeAll()
        if computer.isAttached {
            computer.detach()
        }
        
        deck.putBack(&matchedCards)
        deck.putBack(&cardsInGame)
        
        sessionMode = .none
    }
    
    private static func checkIfPropertyMatches(_ name: String, of cards: [Card]) -> Bool {
        let firstSameAsSecond = cards[0].hasSameProperty(name, as: cards[1])
        let secondSameAsThird = cards[1].hasSameProperty(name, as: cards[2])
        let thirdSameAsFirst = cards[2].hasSameProperty(name, as: cards[0])
        
        return firstSameAsSecond == secondSameAsThird &&
                thirdSameAsFirst == firstSameAsSecond
    }
    
    static func checkIfAllPropertiesMatch(of cards: [Card]) -> Bool {
        var matchChecks = [Bool](repeating: false, count: cards.first!.instancePropertyNames.count)
        
        for (index, name) in cards.first!.instancePropertyNames.enumerated() {
            matchChecks[index] = checkIfPropertyMatches(name, of: cards)
        }
        
        // True only if all of values are true.
        return matchChecks.allSatisfy { $0 }
    }
    
    func checkIfSelectedCardsMatch() -> Bool {
        // Replace cards if they match.
        if currentPlayer.isMatchSelected {
            matchedCards += currentPlayer._selectedCards
            cardsInGame = cardsInGame.filter { !matchedCards.contains($0) }
            putCardsInGame(amount: 3)
            
            return true
        }
        
        return false
    }
    
    func onSelectCard(at index: Int) {
        let selectedCard = cardsInGame[index]
        
        if currentPlayer._selectedCards.count < 3 {
            currentPlayer.onSelect(selectedCard: selectedCard)
        } else {
            if checkIfSelectedCardsMatch() {
                currentPlayer.onMatch()
                
                if players.count == 2 {
                    let otherPlayer = self.players.first(where: { $0 != self.currentPlayer })!
                    otherPlayer.onOpponentWin()
                }
                
                // If player selects already matched card, don't select it.
                if !matchedCards.contains(selectedCard) {
                    currentPlayer.onSelect(selectedCard: selectedCard)
                }
            } else {
                currentPlayer.onMismatch(selectedCard: selectedCard)
            }
        }
    }
    
    func putCardsInGame(amount: Int) {
        cardsInGame += deck.draw(amount: amount)
    }
    
    func forceUpdate() {
        if let events = onCardsInGameChange {
            for event in events {
                event(cardsInGame)
            }
        }
    }
}
