//
//  Card.swift
//  Set
//
//  Created by Marcin Sadowski on 08/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import Foundation

struct Card: Hashable, CustomStringConvertible {
    enum Shape: String, CustomStringConvertible, CaseIterable {
        var description: String { rawValue }
        case diamond, oval, squiggle
    }
    
    enum Color: String, CustomStringConvertible, CaseIterable {
        var description: String { rawValue }
        case red, green, purple
    }
    
    enum Shading: String, CustomStringConvertible, CaseIterable {
        var description: String { rawValue }
        case solid, striped, open
    }
    
    private static var identifierFactory = 0
    private let identifier = Card.getIdentifier()
    
    let number: Int
    let shape: Shape
    let color: Color
    let shading: Shading
    var description: String { "\(color) \(number) \(shading) \(shape)" }
    
    var instancePropertyNames: [String] { Mirror(reflecting: self).children.filter { $0.label != "identifier" }.map { $0.label! } }
    
    private static func getIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    func hasSameProperty(_ name: String, as card: Card) -> Bool? {
        switch name {
        case "number": return number == card.number
        case "shape": return shape == card.shape
        case "color": return color == card.color
        case "shading": return shading == card.shading
        default: return nil
        }
    }
}
