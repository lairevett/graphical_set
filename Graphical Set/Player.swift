//
//  Player.swift
//  Set
//
//  Created by Marcin Sadowski on 10/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import Foundation

class Player: Equatable {
    private let identificator = Player.makeIdentificator()
    
    private var lastMatch: Date?
    private var lastMismatch: Date?
    
    private(set) var score = 0
    var _selectedCards = [Card]()
    
    var isMatchSelected: Bool { _selectedCards.count == 3 && Game.checkIfAllPropertiesMatch(of: _selectedCards) }
    
    private static var identificatorFactory = 0
    private static func makeIdentificator() -> Int {
        identificatorFactory += 1
        return identificatorFactory
    }
    
    static func == (lhs: Player, rhs: Player) -> Bool {
        lhs.identificator == rhs.identificator
    }
    
    func getSelectedCardsIndices(from cardsOnTable: [Card]) -> [Int] {
        _selectedCards.map {
            // When computer/player clicks same match just before player/computer gets it.
            if let index = cardsOnTable.firstIndex(of: $0) {
                return index
            } else {
                return 0
            }
        }
    }
    
    func onSelect(selectedCard: Card) {
        if !_selectedCards.contains(selectedCard) {
            _selectedCards.append(selectedCard)
        } else {
            score -= 1 // Deselection penalty.
            if let index = _selectedCards.firstIndex(of: selectedCard) {
                _selectedCards.remove(at: index)
            } else {
                _selectedCards.removeAll()
            }
        }
    }
    
    func onMatch() {
        // Match reward.
        if lastMatch == nil {
            lastMatch = Date()
            score += 6
        } else {
            let deltaTime = Date().timeIntervalSince(lastMatch!)
            let matchScore = Int(70 - deltaTime)
            score += matchScore > 0 ? matchScore : 1
        }
        
        _selectedCards.removeAll()
    }
    
    func onOverlookedSet() {
        score -= 4
    }
    
    func onMismatch(selectedCard: Card) {
        // Mismatch penalty.
        if lastMismatch == nil {
            lastMismatch = Date()
            score -= 2
        } else {
            let deltaTime = Date().timeIntervalSince(lastMismatch!)
            if deltaTime < 3 {
                score -= 9 // Penalty for guessing.
            } else {
                score -= 3
            }
        }
        
        // Deselect all cards if they aren't matching.
        _selectedCards.removeAll()
        _selectedCards.append(selectedCard)
    }
    
    func onOpponentWin() {
        _selectedCards.removeAll()
    }
}
